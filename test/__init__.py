import unittest
import os
from reproject import app, db
import tempfile
import json


class TestApp(unittest.TestCase):

    username = "test"
    password = "test"
    auth_token = ""

    def test_first(self):
        self.assertEqual(sum([2 + 3]), 5)

    def test_connection(self):
        pass

    def setUp(self):
        """
        Setting up test client
        """
        self.db_fd, app.app.config["DATABASE"] = tempfile.mkstemp()
        app.app.config["TESTING"] = True
        app.app.config["WTF_CSRF_ENABLED"] = False
        app.app.config["DEBUG"] = False
        self.assertEqual(app.app.debug, False)
        self.app = app.app.test_client()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(app.app.config["DATABASE"])

    def test_index_page(self):
        rv = self.app.get("/")
        self.assertIn("Hello", rv.data.decode())

    def test_registration(self):
        """ Test for user registration """
        with self.app:
            db.session.execute(
                "delete from user where username='test'"
            )  # delete test user from after testing
            response = self.app.post(
                "/register",
                data=json.dumps(dict(username=self.username, password=self.password)),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())
            self.assertTrue(data["status"] == "success")
            self.assertTrue(
                data["message"] == f"User {self.username} successfully registered."
            )
            self.assertTrue(response.content_type == "application/json")
            self.assertEqual(response.status_code, 200)

    def test_login(self):
        """
        Test for login route
        """
        with self.app:
            response = self.app.post(
                "/login",
                data=json.dumps(dict(username=self.username, password=self.password)),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())

            self.assertTrue(data["status"] == "success")
            self.assertIn("access_token", data.keys())
            self.auth_token = data["access_token"]
            self.assertTrue(
                data["message"]
                == f"Use attached auth_token in further requests, valid for 1 day"
            )
            self.assertTrue(response.content_type == "application/json")
            self.assertEqual(response.status_code, 200)

    def test_error_when_no_auth_projections(self):
        """
        test that unauthorized user should not be allowed to my request to /projections route
        """
        with self.app:
            response = self.app.get("/projections",)

            data = json.loads(response.data.decode())

            self.assertTrue(data["status"] == "fail")
            # self.assertTrue(data['message'] == f'Use attached auth_token in further requests, valid for 1 day')
            self.assertTrue(response.content_type == "application/json")
            self.assertEqual(response.status_code, 401)

    def test_error_when_no_auth_reproject(self):
        """
        test that unauthorized user should not be allowed to my request to vector/projections route
        """
        with self.app:
            response = self.app.post(
                "/vector/reproject",
                data=dict(
                    target_crs="epsg:4326",
                    geojson=open(
                        "example_vector.geojson",
                        "rb",
                    ),
                ),
                content_type="multipart/form-data",
            )

            data = json.loads(response.data.decode())
            self.assertTrue(data["status"] == "fail")
            # self.assertTrue(data['message'] == f'Use attached auth_token in further requests, valid for 1 day')
            self.assertTrue(response.content_type == "application/json")
            self.assertEqual(response.status_code, 401)

    # def test_auth_projections(self):
    #     '''
    #     TODO:getting value error, asking two values in headers only one given
    #     '''
    #     with self.app:
    #         response = self.app.get(
    #                     '/projections',
    #                     headers=json.dumps(dict(
    #                         Authorization = 'Bearer '+ self.auth_token,
    #                     )),
    #                     data = json.dumps(dict(
    #                         username=self.username,
    #                         password = self.password
    #                     )),
    #                     content_type='json/application'
    #                 )
    #         print(response)
    #         data = json.loads(response.data.decode())
    #         print(data)
    #         self.assertTrue(data['status'] == 'success')
    #         self.assertTrue(data['message'] == f'Use attached auth_token in further requests, valid for 1 day')
    #         self.assertTrue(response.content_type == 'application/json')
    #         self.assertEqual(response.status_code, 200)

    # def test_auth_reproject(self):
    #     '''
    #     TODO:getting value error, asking two values in headers only one given
    #     '''
    #     with self.app:
    #         response = self.app.post(
    #                     '/vector/reproject',
    #                     headers=json.dumps(dict(
    #                         Authorization = 'Bearer '+ self.auth_token,
    #                     )),
    #                     data = dict(
    #                     target_crs='epsg:4326',
    #                     geojson = open('/Users/AK/PersonalProject/gsi-technical-test-python-developer/example_vector.geojson','rb'),
    #         ),
    #                     content_type='multipart/form-data'
    #                 )

    #         data = json.loads(response.data.decode())
    #         self.assertTrue(data['status'] == 'fail')
    #         #self.assertTrue(data['message'] == f'Use attached auth_token in further requests, valid for 1 day')
    #         self.assertTrue(response.content_type == 'application/json')
    #         self.assertEqual(response.status_code, 401)


if __name__ == "__main__":
    unittest.main()
