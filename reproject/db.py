from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker

# from models import Base

conn_string = "sqlite:///db.sqlite"
engine = create_engine(conn_string)
# Base.metadata.create_all(engine)  # here we create all tables
Session = sessionmaker(bind=engine)
session = Session()
