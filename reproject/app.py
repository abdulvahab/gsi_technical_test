#!/usr/bin/env python
from flask import Flask, request, jsonify, make_response
import json
import jwt
import datetime
import passlib.hash
import pyproj
import geojson
import os
from reproject.models import User
from reproject.db import session

app = Flask(__name__)
app.config["SECRET_KEY"] = os.environ.get("SECRET_KEY")


@app.route("/", methods=["GET", "POST"])
def index():
    """Landing page
    :param name:str
    :return greeting:str
    """
    name = request.values.get("name")
    if name == None:
        name = "World"
    response = {'status':'success', 'message':f'<h1>Hello {name}!, Welcome to Geospatial Insight Web API</h1>'}
    return make_response(jsonify(response)), 200


def encrypt_password(password: str) -> str:
    return passlib.hash.pbkdf2_sha256.hash(password)


def verify_password(password: str, encypted_password: str) -> str:
    return passlib.hash.pbkdf2_sha256.verify(password, encypted_password)


def encode_auth_token(user_id):
    """
    Generates the Auth Token
    :return: string auth_token
    """
    try:
        payload = {
            "exp": datetime.datetime.utcnow() + datetime.timedelta(days=1, seconds=5),
            "iat": datetime.datetime.utcnow(),
            "sub": user_id,
        }
        return jwt.encode(payload, app.config.get("SECRET_KEY"), algorithm="HS256")
    except Exception as e:
        return e


def decode_auth_token(auth_token):
    """
    Decodes the auth token
    :param auth_token:
    :return: integer|string
    """
    try:
        payload = jwt.decode(auth_token, app.config.get("SECRET_KEY"))
        return payload["sub"]
    except jwt.ExpiredSignatureError:
        return "Signature expired. Please log in again."
    except jwt.InvalidTokenError:
        return "Invalid token. Please log in again."


def validate_auth_token(auth_header=None):
    """validate auth tocken supplied in header 
    and generate response accordingly
    :param auth_header
    :return response json
    """
    if auth_header:
        auth_token = auth_header.split(" ")[1]
        if auth_token:
            resp = decode_auth_token(auth_token)
            if not isinstance(resp, str):
                response = {"status": "success", "message": resp}
                return response
            else:
                response = {"status": "fail", "message": resp}
                return response

        else:
            responseObject = {
                "status": "fail",
                "message": "Provide a valid auth token.",
            }
            return response
    else:
        response = {
            "status": "fail",
            "message": "Provide a auth token in header.",
        }
        return response


@app.route("/login", methods=["GET", "POST"])
def login():
    """Parse username and password from Post request form data
    generate auth_token if username and password is corrrect
    prompt to register if username doesn't exist
    Error if password is wrong 
    :return auth_token
    """
    post_data = request.get_json()
    print(post_data)
    username = post_data.get("username")
    result = session.execute(f"select * from user where username='{username}'")
    user_info = {}
    for row in result:
        user_info["username"] = row.username
        user_info["password"] = row.encrypted_password
        user_info["userid"] = row.user_id

    if not user_info:
        error = {
            "status": "fail",
            "message": f"error: {username} is not a registered user. Please register using /register endpoint",
        }
        return make_response(jsonify(error)), 401
    else:
        if verify_password(post_data["password"], user_info["password"]):
            mytoken = encode_auth_token(user_info["userid"]).decode("utf-8")
            response = {
                "status": "success",
                "message": "Use attached auth_token in further requests, valid for 1 day",
                "access_token": mytoken,
            }
            return make_response(jsonify(response)), 200
        else:
            error = {
                "status": "fail",
                "message": f"error :entered password is wrong. Please check and try again",
            }
            return make_response(jsonify(error)), 401


@app.route("/register", methods=["GET", "POST"])
def register():
    """register a new user using provided
    username and password parsed from post request 
    """
    post_data = request.get_json()
    username = post_data.get("username")
    password = post_data.get("password")
    crypt_password = encrypt_password(password)
    new_user = User(username=username, encrypted_password=crypt_password)
    session.add(new_user)
    session.commit()
    return (
        make_response(
            jsonify(
                message=f"User {username} successfully registered.", status="success"
            )
        ),
        200,
    )


@app.route("/projections")
def projections():
    """
    Returns the allowed projections for the (authorised) calling user.
    """
    auth_header = request.headers.get("Authorization")
    response = validate_auth_token(auth_header)
    if response["status"] == "success":
        user_id = response["message"]
        result = session.execute(
            f"select epsg_code from user_crs join crs on user_crs.crs_id = crs.crs_id where user_id='{user_id}'"
        )
        epsg_codes = []
        for row in result:
            epsg_codes.append(row.epsg_code)
        return (
            make_response(
                jsonify(
                    {
                        "status": "success",
                        "message": "permitted CRS",
                        "epsg_codes": epsg_codes,
                    }
                )
            ),
            200,
        )
    else:
        return make_response(jsonify(response)), 401


@app.route("/vector/reproject", methods=["GET", "POST"])
def vector_reproject():
    """
    Reprojects a vector between CRSs.
    :param geojson file, target_crs
    :return reprojected geojson file content
    """
    auth_header = request.headers.get("Authorization")
    response = validate_auth_token(auth_header)

    if response["status"] == "success":
        target_crs = request.values.get("target_crs")

        filename = request.files.get("geojson")

        geometry = geojson.load(filename)

        source_crs = geometry["crs"]["properties"]["name"]
        try:
            transformer = pyproj.Transformer.from_crs(
                source_crs, target_crs, always_xy=True,
            )
            reprojected = {
                **geojson.utils.map_tuples(
                    lambda c: transformer.transform(c[0], c[1]), geometry,
                ),
                **{"crs": {"type": "name", "properties": {"name": target_crs}}},
            }
            return make_response(jsonify(reprojected)), 200
        except:
            # Through an error if target_crs is invalid
            return (
                make_response(
                    jsonify(
                        {
                            "status": "fail",
                            "message": f"Invalid projection: {target_crs}:(Internal Proj Error: proj_create: crs not found)",
                        }
                    )
                ),
                400,
            )

    else:
        return make_response(jsonify(response)), 401


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=False)
